#! /bin/bash


keywords="tech+lead"
#keywords="architect"
week=r604800
day=r86400
period=$day

rm pages -rf
mkdir pages

function go()
{
    location=$1
    for offset in $(seq 0 25 500) 
    do
        outfile="pages/${location}_${offset}.html"
        ./saveas "https://www.linkedin.com/jobs/search/?keywords=${keywords}&location=${location}&f_TPR=${period}&start=${offset}" --destination ${outfile};
        if grep 'No matching jobs found.' $outfile; then 
            break 
        fi
    done
}

#go Switzerland
#go Sweden
go European+Economic+Area

deno run -A extract.js
firefox pages/index.html
