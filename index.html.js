export const page = (links) =>
`<!DOCTYPE html>
<html lang="en" class="theme theme--mercado  app-loader--default">
    <head>
        <title>jobs</title>
        <style>
div.job-card-container {border-bottom: solid 1px blue; margin-bottom: 10px;}
div.mlA {display: none}
a.job-card-container__link{font-family: monospace; font-weight: bold; font-size: 1.2em;}
details{border-bottom: dashed 1px gray; margin-bottom: 10px;}
        </style>
        <script type="module">
            const badCountry = /(france|ireland|romania|belgium|bulgaria)/i
            const badTitle = /(java|spring|frontend|front end|support|test|operations|sap|salesforce|QA|PHP)/i
            const goodTitle = /(lead|staff|principal|architect)/i
            let jobs = document.querySelectorAll('div.job-card-container')
            let cntBad = 0
            jobs.forEach(j => 
            {
                let a = j.querySelector('a.job-card-container__link') 
                let country = j.querySelector("ul.job-card-container__metadata-wrapper")?.innerText.split(', ').at(-1)
                let title = a.getAttribute('aria-label')
                let txt = title + ' - ' + country 
                let viewed = Array.from(j.querySelectorAll('li.job-card-container__footer-job-state')).filter(e => ['Viewed', 'Applied'].includes(e.innerText))
                a.target = '_blank'
                a.innerText = txt
                if(viewed.length || badTitle.exec(title) || !goodTitle.exec(title) || badCountry.exec(country))
                {
                    j.parentElement.innerHTML = '<details><summary>'+txt+'</summary>'+j.outerHTML+'</details>'
                    ++cntBad
                }
            })
            document.title = jobs.length - cntBad +' good ' + cntBad + ' bad'
        </script>
    </head>
    <body>
        ${links}
    </body>
</html>`
