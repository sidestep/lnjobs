import { expandGlobSync } from "https://deno.land/std@0.170.0/fs/expand_glob.ts";
import { DOMParser } from "https://deno.land/x/deno_dom/deno-dom-native.ts";
import { page } from './index.html.js'

const read = Deno.readTextFileSync
const write = Deno.writeTextFileSync
const parser = new DOMParser() 

let pages = 'pages/*.html' 
pages = Array.from(expandGlobSync(pages))

console.log(`[~] found ${pages.length} pages`)
let links = []
pages.forEach(p => 
    {
        let doc = parser.parseFromString(read(p.path), 'text/html')        
        let ul = doc.querySelector('div[data-results-list-top-scroll-sentinel]+ul')
        if(ul)
            links.push(ul.outerHTML)
    })

write('pages/index.html', page(links))
console.log(`[~] wrote ${links.length} pages`)
